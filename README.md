# sample01

- Docker イメージ : https://hub.docker.com/_/php
- 公式のチュートリアルサンプル : https://www.php.net/manual/ja/tutorial.firstpage.php
- 公式のコマンドライン実行サンプル : https://www.php.net/manual/ja/features.commandline.usage.php

Docker イメージで PHP 拡張機能を使いたい場合は Docker イメージの説明を参照

コマンドラインの実行

```
docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$PWD":/usr/src/myapp \
  -w /usr/src/myapp \
  php:8.2-apache \
    php src/script.php
```

サーバの起動

```
docker run \
  -d --rm \
  --name php-sample01 \
  -p 80:80 \
  -v "$PWD/src":/var/www/html \
  php:8.2-apache
```

http://localhost:80/hello.php





