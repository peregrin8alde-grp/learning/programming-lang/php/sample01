#!/usr/bin/php
<?php

if ($argc != 2 || in_array($argv[1], array('--help', '-help', '-h', '-?'))) {
?>

これは、ひとつのオプションをとるコマンドラインの PHP スクリプトです。

  使用法:
  <?php echo $argv[0]; ?> <option>

  <option> は出力したい単語です。
  --help, -help, -h, あるいは -? を指定すると、
  このヘルプが表示されます。

<?php
} else {
    echo $argv[1];
}
?>
